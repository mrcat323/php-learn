<?php

namespace Lib;

use PDO;

class User
{
    private $db;

    public $id;
    public $name;
    public $login;
    public $password;

    public function __construct()
    {
        $this->db = new PDO('mysql:host=127.0.0.1;dbname=blog_maga', 'root', 'jojo');
    }

    public function create(array $data) 
    {
        $statement = $this->db->prepare(
            "INSERT INTO users (name, login, password) 
            VALUES(:name, :login, :password)
        ");
        $statement->bindParam(':name', $data['name']);
        $statement->bindParam(':login', $data['login']);
        $statement->bindParam(':password', $data['password']);

        $statement->execute();
    }

    public function find(int $id)
    {
        $statement = $this->db->prepare("SELECT * FROM users WHERE id = :id");
        $statement->bindParam(':id', $id);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);
        foreach ($result as $key => $value) {
            $this->{$key} = $value;
        }

        return $this;
    }

    public function save()
    {
        $statement = $this->db->prepare(
            "UPDATE users SET name = :name, login = :login, password = :password"
        );
        $statement->bindParam(':name', $this->name);
        $statement->bindParam(':login', $this->login);
        $statement->bindParam(':password', $this->password);

        $statement->execute();

        return $this;
    }

    public function delete()
    {
        $statement = $this->db->prepare("DELETE FROM users WHERE id = :id");

        $statement->bindParam(':id', $this->id);
        $statement->execute();

        return null;
    }

    public function get()
    {
        $statement = $this->db->prepare("SELECT * FROM users");
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
}