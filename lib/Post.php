<?php 

namespace Lib;

class Post extends DB
{
    protected $table = 'posts';
    protected $columns = [
        'title',
        'description'
    ];
}